import React, { Component, PropTypes } from 'react';
import { DropTarget } from 'react-dnd';
import classNames from 'classnames';
import itemTypes from '../constants/itemTypes';
import Element from './Element';

class Elements extends Component {
  static propTypes = {
    elements: PropTypes.array.isRequired,
    canDrop: PropTypes.bool.isRequired,
    isOver: PropTypes.bool.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    rowId: PropTypes.number.isRequired,
  }
  // required since after the element has been dropped it will point to the method in Column.js
  pushElement() {
  }

  // required since after the element has been dropped it will point to the method in Column.js
  removeElement() {
  }

  // required since after the element has been dropped it will point to the method in Column.js
  moveElement() {
  }

  // required since after the element has been dropped it will point to the method in Column.js
  alignmentState() {
  }

  render() {
    const { elements, rowId, id } = this.props;
    const { canDrop, isOver, connectDropTarget } = this.props;
    const isActive = canDrop && isOver;

    return connectDropTarget(
      <div
        className={
          classNames('box', 'box-elements', {
            'box__active': isActive,
            'box__inactive': !isActive,
          })}>
        {elements && elements.map((elem, index) => {
          return (
            <Element
              key={elem.id}
              index={index}
              containerId={id}
              elem={elem}
              rowId={rowId}
              removeElement={this.removeElement}
              moveElement={this.moveElement}
              alignmentState={this.alignmentState} />
          );
        })}
      </div>
    );
  }
}

export default DropTarget(itemTypes.ELEMENT, {}, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))(Elements);

import * as actionTypes from '../constants/actionTypes';

export const addRow = row => {
  return {
    type: actionTypes.ADD_ROW,
    row
  };
};

export const removeRow = rowId => {
  return {
    type: actionTypes.REMOVE_ROW,
    rowId
  };
};
export const addColumn = (column, rowId) => {
  return {
    type: actionTypes.ADD_COLUMN,
    column,
    rowId,
  };
};

export const removeColumn = (columnId, rowId) => {
  return {
    type: actionTypes.REMOVE_COLUMN,
    columnId,
    rowId,
  };
};

export const addElement = (element, columnId, rowId) => {
  return {
    type: actionTypes.ADD_ELEMENT,
    element,
    columnId,
    rowId,
  };
};

export const removeElement = (element, columnId, rowId) => {
  return {
    type: actionTypes.REMOVE_ELEMENT,
    element,
    columnId,
    rowId,
  };
};

export const switchElements = (targetElement, sourceElement, columnId, rowId) => {
  return {
    type: actionTypes.SWITCH_ELEMENTS,
    targetElement,
    sourceElement,
    columnId,
    rowId,
  };
};

export const updateElementAlignment = (rowId, columnId, elementUuid, alignment) => {
  return {
    type: actionTypes.UPDATE_ELEMENT_ALIGNMENT,
    rowId,
    columnId,
    elementUuid,
    alignment
  };
};

export const generateHTML = () => {
  return {
    type: actionTypes.GENERATE_HTML,
  };
};

export const clearLayout = () => {
  return {
    type: actionTypes.CLEAR_LAYOUT,
  };
};

export const setBackgroundColor = (color) => {
  return {
    type: actionTypes.SET_BACKGROUND_COLOR,
    color,
  };
};

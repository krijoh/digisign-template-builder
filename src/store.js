import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { createLogger } from 'redux-logger';
import reducers from './reducers';

const rootReducer = combineReducers({ ...reducers });

const middlewares = [];
const enhancers = [];

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    collapsed: true,
    duration: true,
  });

  middlewares.push(logger);

  const devToolsExtension = window.devToolsExtension;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middlewares),
  ...enhancers
);

export default(initialState) => {
  return createStore(rootReducer, composedEnhancers, initialState);
};

import _ from 'lodash';
import * as actionTypes from '../constants/actionTypes';

export default (state = { rows: [], backgroundColor: '#f0f0f0' }, payload) => {
  const { type } = payload;
  switch (type) {
    case actionTypes.ADD_ROW:
      return { ...state, rows: [...state.rows, payload.row] };

    case actionTypes.REMOVE_ROW:
      return { ...state, rows: [...state.rows.filter(row => row.id !== payload.rowId)] };

    case actionTypes.ADD_COLUMN:
      return { ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) return { columns: [...row.columns, payload.column], id: row.id };
          return row;
        })
      };

    case actionTypes.REMOVE_COLUMN:

      return { ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) return { columns: row.columns.filter(column => column.id !== payload.columnId), id: row.id };
          return row;
        })
      };

    case actionTypes.ADD_ELEMENT:
      return {
        ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) {
            return {
              id: row.id,
              columns: row.columns.map(col => {
                if (col.id === payload.columnId) {
                  return { id: col.id, size: col.size, elements: [...col.elements, payload.element] };
                }
                return col;
              }),
            };
          }
          return row;
        })
      };

    case actionTypes.REMOVE_ELEMENT:
      return {
        ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) {
            return {
              id: row.id,
              columns: row.columns.map(col => {
                if (col.id === payload.columnId) {
                  return { id: col.id, size: col.size, elements: col.elements.filter(element => element.uuid !== payload.element.uuid) };
                }
                return col;
              })
            };
          }
          return row;
        })
      };

    case actionTypes.UPDATE_ELEMENT_ALIGNMENT:
      return {
        ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) {
            return {
              id: row.id,
              columns: row.columns.map(col => {
                if (col.id === payload.columnId) {
                  return {
                    id: col.id,
                    size: col.size,
                    elements: col.elements.map(element => {
                      if (element.uuid === payload.elementUuid) {
                        return {
                          id: element.id,
                          uuid: element.uuid,
                          alignment: payload.alignment,
                          text: element.text,
                        };
                      }
                      return element;
                    })
                  };
                }
                return col;
              })
            };
          }
          return row;
        })
      };

    case actionTypes.SWITCH_ELEMENTS:
      return {
        ...state,
        rows: state.rows.map(row => {
          if (row.id === payload.rowId) {
            return {
              id: row.id,
              columns: row.columns.map(col => {
                if (col.id === payload.columnId) {
                  return { id: col.id, size: col.size, elements: switchElements(_.cloneDeep(col), payload.sourceElement, payload.targetElement) };
                }
                return col;
              })
            };
          }
          return row;
        })
      };

    case actionTypes.GENERATE_HTML:
      const { rows } = state;
      let textHtml = `<style>body { background-color: ${state.backgroundColor}; }</style>\n`;
      textHtml += `<div class="container main-content">\n`;

      rows.forEach(row => {
        textHtml += `\t<div class="row">\n`;
        row.columns.forEach(column => {
          textHtml += `\t\t<div class="col-md-${column.size}">\n`;
          const elementsLength = column.elements.length;
          let clearFix = false;

          column.elements.forEach((element, elementIndex) => {
            let className = '';
            const alignment = element.alignment;

            switch (element.text) {
              case 'Text':
                if (alignment === 'center') {
                  className = 'text-center';
                } else if (alignment === 'right') {
                  className = 'text-right';
                }
                textHtml += `\t\t\t<p class="${className}"><asp:Literal runat="server" ID="Text"></asp:Literal></p>\n`;
                textHtml += insertClearfix(clearFix);
                break;

              case 'Image':
                if (alignment === 'center') {
                  className = 'center-block';
                } else if (alignment === 'right') {
                  className = 'pull-right';
                  clearFix = true;
                }
                textHtml += `\t\t\t<asp:Image runat="server" ID="Image" CssClass="${className}" />\n`;
                textHtml += insertClearfix(clearFix);
                break;

              case 'Header':
                if (alignment === 'center') {
                  className = 'text-center';
                } else if (alignment === 'right') {
                  className = 'text-right';
                }
                textHtml += `\t\t\t<h1 class="${className}"><asp:Literal runat="server" ID="Header"></asp:Literal></h1>\n`;
                break;

              case 'Content':
                textHtml += `\t\t\t<div><asp:Literal runat="server" ID="Content"></asp:Literal></div>\n`;
                break;

              case 'RSS':
                textHtml += `\t\t\t<script src="/Scripts/jquery-2.0.3.js"></script>\n\t\t\t<script src="/Scripts/moment.min.js"></script>\n\t\t\t<script src="/Scripts/jquery.rss.min.js"></script>\n\t\t\t<script> jQuery(function($) { $('#rss-feeds').rss('<asp:Literal runat="server" ID="RSS"></asp:Literal>', { limit: 10 })})</script>\n\t\t\t<div class="${className}">\n\t\t\t\t<div id="rss-feeds"></div>\n\t\t\t</div>\n`;
                break;

              case 'Video':
                if (alignment === 'center') {
                  className = 'center-block';
                } else if (alignment === 'right') {
                  className = 'pull-right';
                  clearFix = true;
                }
                textHtml += `\t\t\t<div class="${className}"><iframe id="template-video" width="560" height="315" frameborder="0" allowfullscreen></iframe>\n\t\t\t<script>var youtubeURL = '<asp:Literal runat="server" ID="Video"></asp:Literal>' + '?autoplay=1'; document.getElementById('template-video').src = youtubeURL;</script></div>\n`;
                //textHtml += `\t\t\t<video class="${className}" controls>\n\t\t\t\t<source id="template-video" src="" type="video/mp4">\n\t\t\t</video>\n\t\t\t<script>document.getElementById('template-video').src = ('<asp:Literal runat="server" ID="Video"></asp:Literal>');</script>\n`;
                textHtml += insertClearfix(clearFix);
                break;

              case 'Iframe':
                textHtml += `\t\t\t<div class="template-iframe">\n\t\t\t\t<iframe id="template-iframe-src" frameborder="0" src=""></iframe>\n\t\t\t</div>\n\t\t\t<script>document.getElementById('template-iframe-src').src = ('<asp:Literal runat="server" ID="Iframe"></asp:Literal>');</script>\n`;
                break;

              default:
            }

            if (elementIndex + 1 === elementsLength) {
              textHtml += `\t\t</div>\n`;
            }

            clearFix = false;
          });

          if (elementsLength === 0) {
            textHtml += `\t\t</div>\n`;
          }
        });

        textHtml += '\t</div>\n';
      });

      textHtml += `</div>\n`;
      console.log(textHtml);

      // fill input tag with generated html/asp.net code which is then read by asp.net webforms app
      if (process.env.NODE_ENV !== 'development') {
        const webFormInput = document.getElementById('webFormInput');
        webFormInput.innerHTML = textHtml;
      }
      return state;

    case actionTypes.CLEAR_LAYOUT:
      return { rows: [], backgroundColor: '#f1f1f1' };

    case actionTypes.SET_BACKGROUND_COLOR:
      return { ...state, backgroundColor: payload.color.hex };

    default:
      return state;
  }
};

function switchElements(columnObject, sourceItem, targetItem) {
  const targetIndex = columnObject.elements.map(element => element.uuid).indexOf(targetItem.uuid);
  const sourceIndex = columnObject.elements.map(element => element.uuid).indexOf(sourceItem.uuid);

  columnObject.elements.splice(targetIndex, 1, sourceItem);
  columnObject.elements.splice(sourceIndex, 1, targetItem);
  return columnObject.elements;
}

function insertClearfix(clearFix) {
  if (clearFix) {
    return `\t\t\t<div class="clearfix"></div>\n`;
  }
  return '';
}

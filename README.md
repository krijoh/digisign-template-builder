# Examensarbete app av Kristoffer Johansson

Built with react, react-dnd, redux and bootstrap.  
Build layouts for DigiSign application by Omegapoint using drag-and-drop.  
The generated ASP.NET/HTML code will appear in the browsers console.  
In production the app is integrated with DigiSign using C# and .NET and will be saved as a template in the database.  
Source code is in ./src

## Instructions


### Requirements
- node.js
- npm


To run the application

```
npm install
npm start
```

Then visit http://localhost:3000
